require('dotenv').config()

var express = require('express');
const cors = require('cors');
const jwt = require('./_helpers/jwt.js');
const bodyParser = require('body-parser');
var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
var session = require('express-session');
var MySQLStore = require('express-mysql-session')(session);
var options = {
  host     : process.env.DB_HOST,
  user     : process.env.DB_USER,
  password : process.env.DB_PASSWORD,
  database : process.env.DB_NAME
};

var corsOptions = {
  origin: process.env.FRONTEND_URI,
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}
app.options('*', cors(corsOptions))
app.use(cors(corsOptions));

// use JWT auth to secure the api
app.use(jwt());

var sessionStore = new MySQLStore(options);

app.use(session({
    key: process.env.SESSION_NAME,
    secret: process.env.SESSION_SECRET,
    store: sessionStore,
    resave: false,
    saveUninitialized: false
}));

var UserController = require('./user/user.controller.js');
app.use('/users', UserController);

module.exports = app;
