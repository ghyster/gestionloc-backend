//const expressJwt = require('express-jwt');
const { expressjwt: jwtp } = require("express-jwt");

require('dotenv').config()
const userService = require('../user/user.service');

module.exports = jwt;

function jwt() {
    const secret = process.env.JWT_SECRET;
    return jwtp({ secret, isRevoked ,algorithms: ["HS256"]}).unless({
        path: [
            // public routes that don't require authentication
            '/users/authenticate',
            '/users/register'
        ]
    });
}

async function isRevoked(req, payload, done) {
    //console.log(payload);
    const user = await userService.getById(payload.sub);

    // revoke token if user no longer exists
    if (!user) {
        return done(null, true);
    }

    done();
};
