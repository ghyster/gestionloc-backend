# gestionloc-backend

## Description
Gestionloc backend to manage users, rentals, ...

## Badges
TODO

## Installation
You'll need google [OAuth 2.0 Client IDs](https://developers.google.com/identity/protocols/) to authenticate on the application. A sample is located in user/gestionloc-keys.json.sample. You'll also need Google drive and Google docs API enabled on your project

## Usage
TODO

## Support
You can file issues in this repository

## Roadmap
User creation is disabled for now, when the application is ready, user creation will be possible

## Contributing
Contributions are welcome

## License
TODO

## Project status
project creation
