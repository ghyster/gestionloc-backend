﻿require('dotenv').config()
const jwt = require('jsonwebtoken');

const User = require('./user.js');

const {OAuth2Client} = require('google-auth-library');

const keys = require('./gestionloc-keys.json');

const client = new OAuth2Client(
    keys.web.client_id,
    keys.web.client_secret,
    keys.web.redirect_uris[0]
);    

module.exports = {
    authenticate,
    getById,
    create
};

async function authenticate(authtoken) {
  

  const ticket = await client.verifyIdToken({
      idToken: authtoken,
      audience: keys.web.client_id,  // Specify the CLIENT_ID of the app that accesses the backend
      // Or, if multiple clients access the backend:
      //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
  });
  //console.log(ticket);

  const payload = ticket.getPayload();
  const usermail = payload['email'];
  const user = await User.getUser(usermail);
  //console.log(user);
  const token = jwt.sign({ sub: user.uid }, process.env.JWT_SECRET);
  return {
    token
  };

}

async function getById(id) {
    return await User.findById(id);
}

async function create(userParam) {
    /*// validate
    if (await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    const user = new User(userParam);

    // hash password
    if (userParam.password) {
        user.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // save user
    await user.save();*/
}