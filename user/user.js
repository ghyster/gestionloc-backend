var db = require('../db');

var User = {
    getUser: async function(email, callback)
    {
      var result = await db.query('SELECT id,mail from users WHERE mail=?', [email]);
      return result[0];
    },

    findById: async function(id, callback){
        var result = await db.query('SELECT id, mail FROM users WHERE id=?',[id]);
        return result[0];
    }    
}

module.exports = User;
